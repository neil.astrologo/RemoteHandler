#ifndef _REMOTEHANDLER_HPP_
#define _REMOTEHANDLER_HPP_

#include <cpr/cpr.h>

#include <iostream>
#include <string>

#define TEXT_T 0
#define FILE_T 1
#define POST_T 2
#define CREDS_T 3

std::string removeTrailingSlash(const std::string_view str);

// TEMPORARY
// DEFINE STRUCT FOR REST API CREDENTIALS
typedef struct {
    std::string username;
    std::string password;
} api_creds;

class RemoteHandler {
public:
    RemoteHandler(
        std::string base_url, std::string arch, std::string data_topic = "");

    std::string checkFirmwareVer();
    std::string getContentType(const char* query);
    int getFirmware(std::string filepath);
    int get_creds();  // essentially should do something like this curl
                      // --location '10.158.9.35:8000/api/user/login'
                      // --header 'Content-Type: application/json' --data-raw
                      // '{"email": "'"$email"'", "password":
                      // "'"$password"'"}'
    bool login(std::string& profile);
    int push_data_log(const std::string_view log_filename);

    // private variable getters
    std::string get_url();
    std::string get_topic();
    std::string get_arch();

private:
    std::string base_url, arch;
    std::string username, password;
    std::string data_topic;
    std::string oauth;
    cpr::Timeout REQ_TIMEOUT;

    bool read_auth(const std::string& authfile);
};

#endif