#include "RemoteHandler.hpp"

#include <cpr/cpr.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "easylogging++.h"
#include "file_read.hpp"
#include "nlohmann/json.hpp"

using json = nlohmann::json;

std::string removeTrailingSlash(const std::string_view str) {
    std::string result(str);

    if (!result.empty() && result.back() == '/') {
        result.erase(result.end() - 1);
    }

    return result;
}

RemoteHandler::RemoteHandler(
    std::string base_url, std::string arch, std::string data_topic)
    : base_url(removeTrailingSlash(std::move(base_url))),
      arch(std::move(arch)),
      data_topic(data_topic),
      REQ_TIMEOUT(10000) {
    // potential for multithreading?
    std::filesystem::path creds_path =
        "/home/pi/CARE/painlessmeshboost/conf/api-credentials.txt";

    bool ok = read_auth(creds_path);
}
bool RemoteHandler::read_auth(const std::string& authfile) {
    std::ifstream credentialsFile(authfile);

    if (credentialsFile.is_open()) {
        // file was successfully read
        // read line per line
        std::string line;
        while (std::getline(credentialsFile, line)) {
            // delimit using =
            size_t eq_pos = line.find('=');

            if (eq_pos == std::string::npos) {
                return false;
            }

            std::string key = line.substr(0, eq_pos);
            std::string value = line.substr(eq_pos + 1);

            if (key == "username") username = value;
            if (key == "password") password = value;
        }

        credentialsFile.close();
    }

    return true;
}

// std::string RemoteHandler::checkFirmwareVer() {
//     std::ostringstream query;
//     query << base_url << "/api/firmware?file=" << arch << "-version.txt";

//     std::string firmwarever = "";

//     if (exec(query.str().c_str(), firmwarever, TEXT_T)) {
//         return "no-firmware";
//     }

//     return firmwarever;
// }

// int RemoteHandler::getFirmware(std::string filepath) {
//     std::ostringstream query;
//     query << base_url << "/api/firmware?file=" << arch << "-care-gw.tar.xz";

//     return exec(query.str().c_str(), filepath, FILE_T);
// }

std::string RemoteHandler::checkFirmwareVer() {
    std::ostringstream query;
    const std::string verfile = arch + "-version.txt";
    query << base_url << "/api/firmware/" << verfile;

    cpr::Header header{{"accept", "file/*"}};

    cpr::Response r = cpr::Get(cpr::Url{query.str()}, header);

    // attempt to read json to check if there is something weird
    json response = json::parse(r.text, nullptr, false);

    if (response.is_discarded()) {
        // file is most likely valid and has some text content
        return r.text;
    }
    return "no-firmware";
}

int RemoteHandler::getFirmware(std::string filepath) {
    std::ostringstream query_tarfile, query_md5;
    std::ostringstream filename, md5;
    std::filesystem::path tarfile, md5file, workdir = get_workdir();

    filename << arch << "-care-gw.tar.xz";
    md5 << arch << "-care-gw.md5";

    tarfile = workdir / filename.str();
    md5file = workdir / md5.str();

    std::ofstream of_tar(tarfile, std::ios::binary);
    std::ofstream of_md5(md5file, std::ios::out);

    query_tarfile << base_url << "/api/firmware/" << filename.str();
    query_md5 << base_url << "/api/firmware/" << md5.str();
    cpr::Header header{{"accept", "file/*"}};

    cpr::Response r_tarfile =
        cpr::Download(of_tar, cpr::Url{query_tarfile.str()});
    cpr::Response r_md5 = cpr::Download(of_md5, cpr::Url{query_md5.str()});

    return std::max(r_tarfile.status_code, r_md5.status_code);
}

int RemoteHandler::get_creds() {
    std::ostringstream query;
    query << base_url << "/api/user/login";
    nlohmann::json creds = {{"email", username}, {"password", password}};

    cpr::Header header_creds{
        {"accept", "application/json"}, {"content-type", "application/json"}};

    cpr::Response r =
        cpr::Post(cpr::Url{query.str()}, header_creds, cpr::Body{creds.dump()});

    // retrieve token from API reply
    nlohmann::json j = nlohmann::json::parse(r.text);
    oauth = std::move(j["token"].get<std::string>());
    return r.status_code;
}

bool RemoteHandler::login(std::string& profile) {
    std::ostringstream query;
    query << base_url << "/api/user/profile";

    cpr::Header header_profile{
        {"accept", "application/json"}, {"Authorization", oauth}};

    cpr::Response r = cpr::Get(cpr::Url(query.str()), header_profile);
    nlohmann::json j = nlohmann::json::parse(r.text, nullptr, true);

    if (j.is_discarded()) {
        return false;
    } else {
        profile = j.dump();
        return true;
    }
}

// int RemoteHandler::pushDataLog(
//     const std::string_view dataLogFile, const std::string_view oauth) {
//     std::ostringstream query;
//     query << base_url << "/api/sync/" << data_topic;

//     // set form stuff here
//     std::string postdata = dataLogFile;
//     std::string header = "Authorization: " + oauth;

//     return exec(query.str().c_str(), postdata, POST_T, header.c_str());
// }

int RemoteHandler::push_data_log(const std::string_view log_filename) {
    std::ostringstream query;
    query << base_url << "/api/sync/" << data_topic;
    std::string profile = "";
    LOG(DEBUG) << query.str();

    if (oauth == "") {
        get_creds();
        login(profile);
    }

    cpr::Header sync_headers{
        {"accept", "application/json"},
        {"authorization", oauth},
        {"content-type", "multipart/form-data"}};

    std::string json =
        "/tmp/painlessmeshboost/" + std::string(log_filename) + ".json";
    std::string md5 =
        "/tmp/painlessmeshboost/" + std::string(log_filename) + ".md5";

    // setup multipart form for json
    cpr::Multipart json_part{{"file", cpr::File{json}}};

    cpr::Multipart md5_part{{"file", cpr::File{md5}}};

    cpr::Response r = cpr::Post(cpr::Url(query.str()), sync_headers, json_part);
    LOG(DEBUG) << "JSON file upload status code: " << r.status_code
               << " with message: " << r.text;
    r = cpr::Post(cpr::Url(query.str()), sync_headers, md5_part);
    LOG(DEBUG) << "md5 file upload status code: " << r.status_code
               << " with message: " << r.text;

    return r.status_code;
}

std::string RemoteHandler::get_url() { return base_url; }
std::string RemoteHandler::get_topic() { return data_topic; }
std::string RemoteHandler::get_arch() { return arch; }
